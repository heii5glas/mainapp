'''
    Class : TabMeasure

    Third page in the application
    Where the user will do measurement by
    measurement
'''
from collections import deque
from os import path

from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QTextEdit, QLabel, QPushButton, QMessageBox, QFileDialog, QVBoxLayout, QHBoxLayout

import pathStorage as ps
from DataManagement import Measurement
from LAL.Export import load
from Tabs.TabGeneric import TabGeneric
from compare.compare import Compare
from sort.sorting import sorting

#test

defaultScript = 'defaultScript.txt'

#TODO check if a value is already in the set
class TabMeasure(TabGeneric):
    def __init__(self, parent, previousEnabled, nextEnabled):
        super().__init__(parent, previousEnabled, nextEnabled)

        # Ui Objects
        self.txtSerial = QTextEdit(self)
        self.lblNumber = QLabel('Serial number : ',self)
        self.btnRun = QPushButton('Run',self)
        self.txtClio = QTextEdit(self)
        self.btnOpenScript = QPushButton('open Script', self)

        # Variables
        self.editedFlag = False
        self.measurementPath = path.abspath(path.curdir)
        self.tcpCom = None
        self.tcpCtrl = None

        self.script = deque()
        self.listSerialNumber = list()

        # For the layout of the page
        self.vbox1 = QVBoxLayout()
        self.hbox1 = QHBoxLayout()
        self.hbox2 = QHBoxLayout()
        self.vbox2 = QVBoxLayout()

        # Initialise ui objects
        self.initUI()




    def initialize(self, tcpCtrl, tcpCom):
        self.tcpCom = tcpCom
        self.tcpCtrl = tcpCtrl

    def initUI(self):
        self.txtScript = QTextEdit(self)
        self.btnRun.clicked.connect(self.runBtn)
        self.txtClio.setReadOnly(True)
        self.txtScript.textChanged.connect(self.edited)
        self.btnOpenScript.clicked.connect(self.openScript)

        # Setup layout of the page
        self.layoutSetup()

        # Show the page
        self.show()

    def layoutSetup(self):
        # Serial number elements
        self.vbox2.addStretch(2)
        self.vbox2.addWidget(self.lblNumber)
        self.vbox2.addWidget(self.txtSerial)
        self.vbox2.addWidget(self.btnRun)
        self.vbox2.addStretch(2)

        # Side editors
        self.hbox1.addWidget(self.txtScript)
        self.hbox1.addLayout(self.vbox2)
        self.hbox1.addWidget(self.txtClio)

        # Add space next to "open script" button
        self.hbox2.addWidget(self.btnOpenScript)
        self.hbox2.addStretch(3)

        # Full page
        self.vbox1.addLayout(self.hbox1)
        self.vbox1.addLayout(self.hbox2)

        # Set layout to this page
        self.setLayout(self.vbox1)


    def showWarning(self, message):
        self.msg = QMessageBox()
        self.msg.setIcon(QMessageBox.Warning)
        self.msg.setText(message)
        self.msg.setWindowTitle("Warning")
        self.msg.setStandardButtons(QMessageBox.Ok)
        self.msg.exec_()

    def showResult(self, message):
        self.msg = QMessageBox()
        self.msg.setIcon(QMessageBox.Warning)
        self.msg.setText("Result : \n" + message)
        self.msg.setWindowTitle("Result")
        self.msg.setStandardButtons(QMessageBox.Ok)
        self.msg.exec()

    def sameSerialNumber(self):
        self.msg = QMessageBox()
        self.msg.setIcon(QMessageBox.Warning)
        self.msg.setText("This serial number is already used... Do you want to overwrite measures for this speaker?")
        self.msg.setWindowTitle("Warning")
        self.msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        if self.msg.exec() == QMessageBox.Yes:
            return True
        else:
            return False


    def runBtn(self):
        if self.tcpCtrl.connected:
            self.tcpCom.checkFolder()
            if self.txtSerial.toPlainText() and self.txtScript.toPlainText():
                self.txtClio.clear()
                #self.txtClio.append(self.tcpCtrl.runBtn(self.txtSerial.toPlainText(), self.txtScript.toPlainText()))
                if self.txtSerial.toPlainText() in self.listSerialNumber:
                    response = self.sameSerialNumber()
                    if response:
                        self.tcpCtrl.runBtn(self.txtScript.toPlainText())
                    else:
                        # skip if user don't want to erase previous measure of a specific serial number
                        return None
                else:
                    self.tcpCtrl.runBtn(self.txtScript.toPlainText())

                while self.tcpCtrl.waiting == False and self.tcpCtrl.error == False:
                    temp = self.tcpCtrl.processEvent()
                    if temp != "" :
                        #test pour ne pas avoir le buffer plein
                        # TODO : le if est un test
                        #if temp != "200 OK\r\n":
                            nbr = temp.find('\r')
                            # remove carriage return from temp
                            temp = temp[0:nbr]
                            self.txtClio.append(temp)
                            # Refresh QApplication to see modifications in txtClio
                            QCoreApplication.processEvents()
                if self.tcpCtrl.error:
                    self.showWarning("There is a sending or a reception error... The connection is stopped")
                    self.tcpCtrl.error = False
                else:
                    #when measurements are finished without error, create the measurementPath and show result
                    # TODO : delete previous file when we know where they are.
                    self.listSerialNumber.append(self.txtSerial.toPlainText())
                    self.tcpCtrl.setMeasurementPath(self.txtSerial.toPlainText())
                    #TODO : sépare le résultat des valeurs de retour
                    self.showResult(self.tcpCom.clioResult)

            elif self.txtSerial.toPlainText() == "":
                self.showWarning("The speaker must have a serial number...")
            elif self.txtScript.toPlainText() == "":
                self.showWarning("There is no script to run...")

            if self.editedFlag == True:
                self.saveScript()
        else:
            self.showWarning("There is no connection to the server...")

    def openScript(self):
        # QFileDialog for choosing Scripts in hard Disk
        test = QFileDialog()
        #return a QString tuple (kind of array with path and extension)
        file = test.getOpenFileName(self, "Get Script", "Scripts", "Text Files (*txt)")
        #to get only the path
        file = str(file[0])
        if (file != ""):
            self.loadScript(file)
            self.saveScript()


    def loadScript(self, file:str = None):
        file = file or ps.WorkingPath + defaultScript
        self.txtScript.setText(self.tcpCom.openScript(file))
        self.txtScript.verticalScrollBar().setValue(0)


    def saveScript(self, file:str = None):
        file = file or ps.WorkingPath + defaultScript
        f = open(file, 'w')
        f.write(self.txtScript.toPlainText())
        f.close()


    def edited(self):
        self.editedFlag = True

    def onNext(self):
        # This is where the compare for a pair list is done
        # The sorting of this list is done also
        try:
            #self.tcpCom.checkFolder()

            # Read lines of measurement path file
            f_mp = open(ps.WorkingPath + 'MeasurementPath.txt')
            s_mp = f_mp.read()
            f_mp.close()

            # Get all the file paths from the list of file paths
            measureList = load(s_mp)
            if measureList is None or len(measureList) == 0:
                print("No measurments could be read")
                return False

            # Go through the data series to get pair list
            c = Compare()
            f = open(file=ps.WorkingPath + 'compare.cfg', mode='r')
            c.loadSettings(f.read())

            # Export list of pairs
            data = Measurement.readList(ps.WorkingPath, measureList)
            if data is None or len(data) == 0:
                print("Data couldn't be loaded")
                return False

            pairs = c.comparelist(data)

            # Write every possible pair to csv file
            file = open(ps.WorkingPath + "pairs.csv", "w")
            for p in pairs:
                file.write(p.printPair())

            sorting(pairs, ps.WorkingPath + "finalPairs.csv")

            return True
        except IOError:
            print("No Measurments")
            return False

    def updateContent(self):
        self.loadScript()
        #self.tcpCom.cleanFolder()
