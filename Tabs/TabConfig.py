'''
    Class : TabConfig

    Second page in the application
    Contains a table which manages the
    measurement configurations

'''
from Tabs.TabGeneric import TabGeneric
from PyQt5.QtWidgets import QTableWidget, QLabel, QPushButton, QComboBox, QTableWidgetItem, QMessageBox, \
    QAbstractItemView, QTableWidgetSelectionRange, QFileDialog, QVBoxLayout, QHBoxLayout
from PyQt5.QtCore import QCoreApplication
from compare.compare import CompareSetting
from LAL import Export
from compare.functions import compareFuncts as cf
import pathStorage as ps
defaultFileName = "compare.cfg"


class TabConfig(TabGeneric):
    def __init__(self, parent, previousEnabled, nextEnabled):
        super().__init__(parent, previousEnabled, nextEnabled)
        self.tblRange = QTableWidget(1, 5, self)  # Table containing configurations
        self.btnAdd = QPushButton('Add', self)  # Button that can add a line to the table
        self.btnRemove = QPushButton('Remove', self)  # Button that removes a line
        self.btnLoadConfig = QPushButton('load config', self)
        self.btnSaveConfig = QPushButton('save config', self)
        self.editAllow = True  # This ensures that if a wrong value was entered, you can't quit the cell
        self.labels = []

        # Layout variables
        self.vbox = QVBoxLayout()
        self.hbox1 = QHBoxLayout()
        self.hbox2 = QHBoxLayout()

        # Setup the whole interface of the page
        self.initUI()



    def initUI(self):
        # Labels for the top cells of the table
        lblLabel = QLabel('Label')
        lblType = QLabel('Type')
        lblF1 = QLabel('f1 [Hz]')
        lblF2 = QLabel('f2 [Hz]')
        lblWeight = QLabel('Weight')
        lblLabel.setStyleSheet('font-weight: bold')
        lblType.setStyleSheet('font-weight: bold')
        lblF1.setStyleSheet('font-weight: bold')
        lblF2.setStyleSheet('font-weight: bold')
        lblWeight.setStyleSheet('font-weight: bold')

        # Add these labels to the table : row, column, widget
        self.tblRange.setCellWidget(0, 0, lblLabel)
        self.tblRange.setCellWidget(0, 1, lblType)
        self.tblRange.setCellWidget(0, 2, lblF1)
        self.tblRange.setCellWidget(0, 3, lblF2)
        self.tblRange.setCellWidget(0, 4, lblWeight)


        # Set actions to execute when the buttons are pressed
        self.btnAdd.pressed.connect(self.addItem)
        self.btnRemove.pressed.connect(self.removeItem)
        self.btnLoadConfig.clicked.connect(self.loadConfig)
        self.btnSaveConfig.clicked.connect(self.saveConfig)

        # Set actions to check the correct type has been entered into cell
        # floats for frequency or weight
        self.tblRange.cellChanged.connect(self.checkTypes)
        self.tblRange.currentCellChanged.connect(self.restrictEdit)

        # Setup the layout
        self.layoutSetup()

        # Show page
        self.show()

    def layoutSetup(self):
        # Top buttons
        self.hbox1.addWidget(self.btnLoadConfig)
        self.hbox1.addWidget(self.btnSaveConfig)

        # Bottom buttons
        self.hbox2.addWidget(self.btnAdd)
        self.hbox2.addWidget(self.btnRemove)

        # Vertical total layout
        self.vbox.addLayout(self.hbox1)
        self.vbox.addWidget(self.tblRange)
        self.vbox.addLayout(self.hbox2)

        # Set the layout to the window
        self.setLayout(self.vbox)


    def addItem(self):
        # Insert row into the table
        self.tblRange.insertRow(self.tblRange.rowCount())

        # The first element of the row is a combobox
        combo1 = QComboBox()
        for label in self.labels:
            combo1.addItem(label)
        combo1.setEditable(True)  # Set the combobox as editable
        combo1.setDuplicatesEnabled(False)

        combo1.currentTextChanged.connect(self.newLabel)

        combo2 = QComboBox()

        for key in cf.keys():
            combo2.addItem(key)

        # Add the combobox to the latest row possible : row, column, widget
        self.tblRange.setCellWidget(self.tblRange.rowCount() - 1, 0, combo1)
        self.tblRange.setCellWidget(self.tblRange.rowCount() - 1, 1, combo2)

    def newLabel(self, text):
        self.labels.clear()
        #get the list of all currently seen items
        for i in range(1, self.tblRange.rowCount()):
            self.labels.append(self.tblRange.cellWidget(i, 0).currentText())

        #and add them back
        for i in range(1, self.tblRange.rowCount()):
            # remove all items but current one
            j = 0
            while not (j == self.tblRange.cellWidget(i,0).count() and j == 1 or self.tblRange.cellWidget(i,0).currentIndex() == -1):

                count = self.tblRange.cellWidget(i,0).count()
                index = self.tblRange.cellWidget(i,0).currentIndex()
                if not j == self.tblRange.cellWidget(i,0).currentIndex():
                    self.tblRange.cellWidget(i, 0).removeItem(j)
                else:
                    j += 1

            # add all the event
            for label in self.labels:
                if not label == self.tblRange.cellWidget(i, 0):
                    self.tblRange.cellWidget(i, 0).addItem(label)

    def removeItem(self):
        # This makes sure the titles don't get removed
        if self.tblRange.currentRow() > 0:
            self.tblRange.removeRow(self.tblRange.currentRow())

    # Checks if values in columns are set correctly
    # Floats for frequencies and weight
    def checkTypes(self, r, c):
        # If it's not the first row (titles)
        if (r > 0):
            # Get string from cell
            item = self.tblRange.item(r, c)
            s_value = item.text()

            if s_value == '':
                return

            # If it's a frequency or weight, it must be a float
            if (c == 2 or c == 3 or c == 4):
                # Check if it isn't a float
                try:
                    float(s_value)
                    self.editAllow = True
                except ValueError:
                    self.showError("Value must be a number")
                    self.editAllow = False

    # Creates a dialog box will show an error message
    def showError(self, text):
        error = QMessageBox(self)
        error.setIcon(QMessageBox.Warning)
        error.setText(text)
        error.setWindowTitle("ERROR")
        error.setStandardButtons(QMessageBox.Ok)
        error.show()

    def restrictEdit(self, newR, newC, oldR, oldC):
        if not self.editAllow:
            self.showError("Value must be a float")

            # MAKES SURE IT IS NOT CALLED RECURSIVELY
            self.tblRange.currentCellChanged.disconnect(self.restrictEdit)
            self.tblRange.setCurrentCell(oldR, oldC)
            self.tblRange.currentCellChanged.connect(self.restrictEdit)

    def onNext(self):
        ok = self.GuiToFile()
        if not ok:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Config not correct do you want to use collect the data anyway")
            msg.setWindowTitle("Error")
            msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            msg.setDefaultButton(QMessageBox.No)
            ret = msg.exec()

            if ret == QMessageBox.Yes:
                ok = True

        return ok

    def loadConfig(self):
        # QFileDialog for choosing Scripts in hard Disk
        test = QFileDialog()
        # return a QString tuple (kind of array with path and extension)
        file = test.getOpenFileName(self, "Load config", "Configs", "Configuration files (*.cfg)")
        # to get only the path
        file = str(file[0])
        if (file != ""):
            self.fileToGui(file)

    def saveConfig(self):
        # QFileDialog for choosing Scripts in hard Disk
        test = QFileDialog()
        # return a QString tuple (kind of array with path and extension)
        file = test.getSaveFileName(self, "Save config", "Configs", "Configuration files (*.cfg)")

        # to get only the path
        file = str(file[0])
        if (file != ""):
            if len(file.split('.')) == 0:
                file = file + ".cfg"
            self.GuiToFile(file)

    def fileToGui(self, file: str = None):

        while self.tblRange.rowCount() > 1:
            self.tblRange.removeRow(1)

        try:
            file = file or (ps.WorkingPath + defaultFileName)
            f = open(file)

            settings = Export.load(f.read())

            f.close()

            if isinstance(settings, list) and len(settings) > 0:
                for s in settings:
                    self.addItem()
                    self.tblRange.cellWidget(self.tblRange.rowCount() - 1, 0).setCurrentText(s.label)
                    self.tblRange.cellWidget(self.tblRange.rowCount() - 1, 1).setCurrentText(s.type)
                    self.tblRange.setItem(self.tblRange.rowCount() - 1, 2, QTableWidgetItem(str(s.inputRange[0])))
                    self.tblRange.setItem(self.tblRange.rowCount() - 1, 3, QTableWidgetItem(str(s.inputRange[1])))
                    self.tblRange.setItem(self.tblRange.rowCount() - 1, 4, QTableWidgetItem(str(s.weight)))
        except IOError:
            print('file not existing')


    def GuiToFile(self, file: str = None):
        settings = []

        for r in range(1, self.tblRange.rowCount()):
            label = None
            type = "rmse"
            weight = None
            inputRangeLow = None
            inputRangeHigh = None

            for c in range(0, self.tblRange.columnCount()):
                obj = self.tblRange.item(r, c)
                wid = self.tblRange.cellWidget(r, c)

                if obj is None and wid is None:
                    return False
                elif c == 0:
                    label = wid.currentText()
                elif c == 1:
                    type = wid.currentText()
                elif c == 2:
                    inputRangeLow = float(obj.text())
                elif c == 3:
                    inputRangeHigh = float(obj.text())
                elif c == 4:
                    weight = float(obj.text())

            settings.append(CompareSetting(label, type, weight, [inputRangeLow, inputRangeHigh]))
        try:
            file = file or (ps.WorkingPath + defaultFileName)
            f = open(file, "w")
            f.write(Export.export(settings))
            f.close()
        except IOError:
            print("File couldn't be open")

        return True

    def updateContent(self):
        # load the default file reloading the previous config.
        self.fileToGui()
