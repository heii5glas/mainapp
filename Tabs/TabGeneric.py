from PyQt5.QtWidgets import QWidget


class TabGeneric(QWidget):

    def __init__(self, parent,previousEnabled, nextEnabled, ):
        super().__init__(parent)
        self.previousEnabled = previousEnabled
        self.nextEnabled = nextEnabled

    def onNext(self):
        return True


    #this methode allow to update the content of a tab when it's opened, it's mainly a convinence on the show without needing
    #to call the super function
    def updateContent(self):
        pass
