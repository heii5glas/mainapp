'''
    Class : TabConnect

    First page in the application
    Manages the TCP connection

'''
from Tabs.TabGeneric import TabGeneric
from PyQt5.QtWidgets import QWidget, QLabel, QTextEdit,QLineEdit, QPushButton, QFileDialog, QVBoxLayout, QHBoxLayout
from DataAcc.TCPCom import TCPCom
from DataAcc.TCPController import TCPController
from os import path as p
import pathStorage as ps

class TabConnect(TabGeneric):
    def __init__(self, parent, previousEnabled, nextEnabled):
        super().__init__(parent, previousEnabled, nextEnabled)
        self.lblIP = QLabel('IP Address', self)
        self.txtIP = QTextEdit('localhost', self)

        self.lblPort = QLabel('Port', self)
        self.txtPort = QTextEdit('1234', self)

        self.txtClio = QTextEdit('Not Connected', self)

        self.btnConnect = QPushButton('Connect', self)
        self.btnConnect.clicked.connect(self.connectBtn)

        self.lblWorkspace = QLabel("Workspace :")
        self.btnBrowse = QPushButton('Browse', self)
        self.btnBrowse.clicked.connect(self.browseBtn)

        self.txtPath = QLineEdit(p.abspath(p.curdir + "/Measurements/"), self)

        #self.connected = False

        self.tcpCom = None
        self.tcpCtrl = None

        # Layout variables
        self.vbox1 = QVBoxLayout()
        self.vbox2 = QVBoxLayout()
        self.hbox1 = QHBoxLayout()
        self.hbox2 = QHBoxLayout()
        self.hbox3 = QHBoxLayout()
        self.hbox4 = QHBoxLayout()

        self.initUI()

    def initialize(self, tcpCtrl, tcpCom):
        self.tcpCom = tcpCom
        self.tcpCtrl = tcpCtrl
        self.tcpCom.initialize(self.tcpCtrl)
        self.tcpCtrl.initialize(self.tcpCom)

    def initUI(self):
        self.txtIP.setMaximumHeight(50)
        self.txtPort.setMaximumHeight(50)
        self.layoutSetup()
        self.show()

    def layoutSetup(self):
        # Top line
        self.hbox1.addWidget(self.lblIP)
        self.hbox1.addStretch()
        self.hbox1.addWidget(self.txtIP)

        # Second line
        self.hbox2.addWidget(self.lblPort)
        self.hbox2.addStretch()
        self.hbox2.addWidget(self.txtPort)

        # Left section
        self.vbox1.addStretch()
        self.vbox1.addLayout(self.hbox1)
        self.vbox1.addStretch()
        self.vbox1.addLayout(self.hbox2)
        self.vbox1.addStretch()
        self.vbox1.addWidget(self.btnConnect)
        self.vbox1.addStretch()

        # Top section
        self.hbox3.addLayout(self.vbox1)
        self.hbox3.addWidget(self.txtClio)

        # Bottom line
        self.hbox4.addWidget(self.btnBrowse)
        self.hbox4.addWidget(self.txtPath)
        self.hbox4.addStretch()

        # Total layout
        self.vbox2.addStretch()
        self.vbox2.addLayout(self.hbox3)
        self.vbox2.addStretch()
        self.vbox2.addWidget(self.lblWorkspace)
        self.vbox2.addLayout(self.hbox4)
        self.vbox2.addStretch()

        # Set the layout to the window
        self.setLayout(self.vbox2)

    def connectBtn(self):
        if self.tcpCtrl.connected == False:
            temp = self.tcpCtrl.connectBtn(self.txtIP.toPlainText(), int(self.txtPort.toPlainText()))
            self.txtClio.setText(temp)
            #self.connected = self.tcpCtrl.connected

    def onNext(self):
        '''
        if self.connected:
            self.txtClio.setText(self.txtPath.text())
        '''

        ps.setWorkingPath( self.txtPath.text())

        self.tcpCom.setOutputFolder(ps.WorkingPath, ps.WorkingPath + 'MeasurementPath.txt')
        return True

    #to choose the where all the data measurements have to be saved
    def browseBtn(self):
        test = QFileDialog()
        # return a QString tuple (kind of array with path and extension)
        folder = test.getExistingDirectory(self, "Working directory", self.txtPath.text())

        if (folder != ""):
            self.txtPath.setText(folder)


