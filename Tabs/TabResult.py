'''
    Class : TabResult

    Fourth page in the application
    Where the user will view the resulting pairs
'''
from Tabs.TabGeneric import TabGeneric
from PyQt5.QtWidgets import QWidget, QTableWidget, QLabel, QVBoxLayout
from PyQt5.QtWidgets import QWidget, QTableWidget, QLabel
import pathStorage as path


class TabResult(TabGeneric):
    def __init__(self, parent, previousEnabled, nextEnabled):
        super().__init__(parent, previousEnabled, nextEnabled)
        self.tblResults = QTableWidget(25, 3, self)
        self.vbox = QVBoxLayout()
        self.initUI()

    def initUI(self):
        # Set title lines
        lblE1 = QLabel("Speaker 1")
        lblE2 = QLabel("Speaker 2")
        lblScore = QLabel("Score")
        lblE1.setStyleSheet('font-weight: bold')
        lblE2.setStyleSheet('font-weight: bold')
        lblScore.setStyleSheet('font-weight: bold')
        self.tblResults.setCellWidget(0, 0, lblE1)
        self.tblResults.setCellWidget(0, 1, lblE2)
        self.tblResults.setCellWidget(0, 2, lblScore)

        # Stretch the table to fit widget
        self.adjustSize()

        # Layout setup
        self.layoutSetup()
        self.show()

    def layoutSetup(self):
        self.vbox.addWidget(self.tblResults)
        self.setLayout(self.vbox)

    def fillTable(self):
        try:
            file = open(path.WorkingPath + 'finalPairs.csv', 'r')

            # Read every file line and adjust row count
            list = file.readlines()
            self.tblResults.setRowCount(len(list)+1)

            # Go through every line in the file put into table (except title lines)
            for i in range(1,self.tblResults.rowCount()) :
                line = list[i-1].split(',')
                self.tblResults.setCellWidget(i,0,QLabel(line[0]))
                self.tblResults.setCellWidget(i,1,QLabel(line[1]))
                self.tblResults.setCellWidget(i,2,QLabel(line[2]))  
        except IOError:
            print('final pairs not found')

    def updateContent(self):
        self.fillTable()

