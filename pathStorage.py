from os import path, makedirs
WorkingPath = ""

def setWorkingPath(p : str):
    global WorkingPath
    WorkingPath = path.abspath(p) + '\\'
    if not path.isdir(WorkingPath):
        makedirs(WorkingPath)
