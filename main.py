import sys
from PyQt5.QtWidgets import QWidget, QApplication, QPushButton #, QLabel, QTextEdit

from Tabs.TabConnect import TabConnect
from Tabs.TabConfig import TabConfig
from Tabs.TabMeasure import TabMeasure
from Tabs.TabResult import TabResult


from DataAcc.TCPCom import TCPCom
from DataAcc.TCPController import TCPController


#   Class defining the application of the window
class TabManager(QWidget):

    #TODO create box layout for tab, buttons, Complex because each tab is a different widget
    def __init__(self):
        super().__init__()
        self.pageIndex = 0
        self.tabs = [TabConnect(self, False, True),   # Connect page
                     TabConfig(self, True, True),   # Configurations page
                     TabMeasure(self, True, True),  # Measure page
                     TabResult(self, True, False)]   # Results page

        self.btnNext = QPushButton('Next', self)            # Next button
        self.btnPrevious = QPushButton('Previous', self)    # Previous button
        self.initUI()                                       # Initialise the interfaces

        #For TCPCom
        self.tcpCom = TCPCom()
        self.tcpCtrl = TCPController()

        self.tabs[0].initialize(self.tcpCtrl, self.tcpCom)
        self.tabs[2].initialize(self.tcpCtrl, self.tcpCom)


    # Initialise all the ui data: button functions, titles etc
    def initUI(self):
        self.setGeometry(700, 150, 1000, 600)
        self.setWindowTitle('Pair Compare')
        self.setGuiGeometries(0)
        self.btnNext.pressed.connect(self.nextPage)
        self.btnPrevious.pressed.connect(self.previousPage)
        self.btnPrevious.setDisabled(True)
        self.resizeEvent = self.setGuiGeometries        # every time the window is resized, setGuiGeometries is called
        self.show()
#        self.tabs[0].show()

        for i in range(1, len(self.tabs)):
            self.tabs[i].hide()




    # Set the sizes of different elements according to window size
    def setGuiGeometries(self, event):
        self.btnNext.setGeometry(self.width()-250, self.height()-75, 200, 50)
        self.btnPrevious.setGeometry(self.width() - 450, self.height() - 75, 200, 50)
        for tab in self.tabs:
            tab.setGeometry(0, 0, self.width(), self.height()-85)

    # Switch to next page
    def nextPage(self):

        # Change the page index if allowed to
        if self.tabs[self.pageIndex].onNext():
            self.pageIndex = self.pageIndex + 1
            self.updatePage()

        #if the connection isn't established, don't go to next page



    # Switch to previous page
    def previousPage(self):
        # Hide the current page pages
        self.tabs[self.pageIndex].hide()

        # Change the page index
        self.pageIndex = self.pageIndex - 1
        self.updatePage()

    def updatePage(self):
        #hide all pages
        for tab in self.tabs:
            tab.hide()

        # Then show current page, avoiding directional problems
        # with next and previous
        self.tabs[self.pageIndex].show()
        self.tabs[self.pageIndex].updateContent()

        self.btnNext.setEnabled(self.tabs[self.pageIndex].nextEnabled)
        self.btnPrevious.setEnabled(self.tabs[self.pageIndex].previousEnabled)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = TabManager()
    sys.exit(app.exec_())
