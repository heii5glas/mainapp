from cx_Freeze import setup, Executable
# this File is used to build a stand alone executable the executable
# Dependencies are automatically detected, but it might need
# fine tuning. It will generate something way too large
buildOptions = dict(packages = ["yaml","DataManagement","DataAcc", "compare", "sort", 'numpy.core._methods', 'numpy.lib.format'], excludes = [])

base = None

executables = [
    Executable('main.py', base=base, targetName = 'PairCompare.exe')
]

setup(name='ClioCom',
      version = '0.1',
      description = 'Pair Compare',
      options = dict(build_exe = buildOptions),
      executables = executables)

